import { createSSRApp } from "vue";
import uviewPlus from '@/uni_modules/uview-plus'

import App from "./App.vue";
export function createApp() {
  const app = createSSRApp(App);
  app.use(uviewPlus)
  return {
    app,
  };
}
